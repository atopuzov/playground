class Solution:
    # @return a boolean
    def isValid(self, s):
        valid = {
            ')': '(',
            '}': '{',
            ']': '['
        }
        end = valid.keys()
        start = valid.values()
        stack = []
        for c in s:
            if c in start:
                stack.append(c)
            elif c in end:
                if stack:
                    last = stack[-1]
                    if last == valid[c]:
                        stack.pop()
                        continue
                return False


        if stack:
            return False
        return True

def main():
    a = Solution()
    print a.isValid(')}{({))[{{[}')


if __name__ == '__main__':
    main()