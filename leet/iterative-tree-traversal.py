# Definition for a  binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    def inorderTraversal(self, root):
        nodes = []
        stack = []
        current = root

        while True:
            if current is not None:
                stack.insert(0, current)
                current = current.left
            else:
                if stack:
                    current = stack.pop(0)
                    nodes.append(current.val)
                    current = current.right
                else:
                    break

        return nodes

    def preorderTraversal(self, root):
        nodes = []
        stack = []
        current = root

        while True:
            if current is not None:
                nodes.append(current.val)
                stack.insert(0, current)
                current = current.left
            else:
                if stack:
                    current = stack.pop(0)
                    current = current.right
                else:
                    break

        return nodes

    def postorderTraversal(self, root):
        pass


def build_sample():
    node1 = TreeNode(1)
    node2 = TreeNode(2)
    node3 = TreeNode(3)

    node1.right = node2
    node2.left = node3
    return node1

def build_sample2():
    '''Wikipedia sample'''
    nodeA = TreeNode('A')
    nodeB = TreeNode('B')
    nodeC = TreeNode('C')
    nodeD = TreeNode('D')
    nodeE = TreeNode('E')
    nodeF = TreeNode('F')
    nodeG = TreeNode('G')
    nodeH = TreeNode('H')
    nodeI = TreeNode('I')

    nodeF.left = nodeB
    nodeF.right = nodeG
    nodeB.left = nodeA
    nodeB.right = nodeD
    nodeD.left = nodeC
    nodeD.right = nodeE
    nodeG.right = nodeI
    nodeI.left = nodeH

    return nodeF

def main():
    r = build_sample2()
    s = Solution()
    inord = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I']
    preord = ['F', 'B', 'A', 'D', 'C', 'E', 'G', 'I', 'H']
    postord = ['A', 'C', 'E', 'D', 'B', 'H', 'I', 'G', 'F']

    print s.inorderTraversal(r) == inord
    print s.preorderTraversal(r) == preord


if __name__ == '__main__':
    main()