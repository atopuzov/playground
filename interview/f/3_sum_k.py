#!/usr/bin/env python
# (c) Aleksandar Topuzovic <Aleksandar.topuzovic@gmail.com>
import random

def generate_random_list(n=10):
    return [random.randint(-100,100) for _ in xrange(n)]


def three_sum_zero(input_array, target_sum=0):
    '''
    Find triplets of
    a b c d e f g h i j k l m n o p q
    ^ ^                             ^
    | |                             |
    i j                             k

    1* Sort the array
    2* Start loop for each element of array:
       sum the element with the first and the last element of the subarray
       if the sum is greater to the targeted one move k to left
       if the sum is smaller to the targeted one move j to right
       if the sum is equeal to the targeted sum then record the result
    '''
    solution = set()
    if input_array is None:  # Not defined on an empty array
        return solution

    if len(input_array) < 3:  # Not defined for an array less then 3 elements
        return solution

    s = sorted(input_array)  # Sort the array, nlog(n)
    n = len(s)

    for i in xrange(n-2):  # Loop trough all the elements except the last 2
        j = i + 1
        k = n - 1

        while j<k:
            A = s[i]
            B = s[j]
            C = s[k]
            current_sum = A + B + C  # Current SUM

            if current_sum == target_sum:  # Sum equals the requested SUM
                solution.add((A, B, C))
                j+=1
                k-=1
            elif current_sum < target_sum:  # Sum is less then the requested SUM, move lower to next
                j+=1
            elif current_sum > target_sum:  # Sum is greater then the requested SUM, move upper to previous
                k-=1

    return solution


def main():
    l = generate_random_list()
    # l = [-1,-1,-1,0,2,2]
    l = [-4,-2,-2,-2,0,1,2,2,2,3,3,4,4,6,6]
    print "Numbers: {}".format(', '.join(str(x) for x in l))
    solution = three_sum_zero(l)
    verify = map(lambda x: x[0]+x[1]+x[2] == 0, solution)

    if verify and all(verify):
        print "All good!"
    else:
        if verify:
            print "Something failed!"
        else:
            print "No solution!"

    print '\n'.join('({}, {}, {})'.format(int(a), int(b), int(c)) for (a,b,c) in solution)

if __name__ == '__main__':
    main()
