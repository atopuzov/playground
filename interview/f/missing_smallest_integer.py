#!/usr/bin/env python
# (c) Aleksandar Topuzovic <Aleksandar.topuzovic@gmail.com>

class Solution:
    # @param A, a list of integers
    # @return an integer
    def firstMissingPositive(self, A):
        i = 0
        while i < len(A):
            print i, A
            if (
                    A[i] >= 1 and  # Don't consider elements <= 0
                    A[i] <= len(A) and  # Don't consider elements that are larger than array size
                    A[i] != i + 1 and  # Next is not this + 1
                    A[A[i] - 1] != A[i]  # Element is not in the right spot
            ):
                # print "Swaping A[i]={}, A[A[i] - 1] = {}".format(A[i], A[A[i] - 1])
                A[A[i] - 1], A[i] = A[i], A[A[i] - 1]  # Swaping
            else:
                i += 1

        for i in xrange(len(A)):
            # print i, A[i]
            if A[i] != i + 1:
                return i + 1

        return len(A) + 1


def main():
    sol = Solution()

    l = [2, 3, 7, 6, 8, -1, -10, 15]
    print sol.firstMissingPositive(l)

if __name__ == '__main__':
    main()
