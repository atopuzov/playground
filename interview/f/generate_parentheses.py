#!/usr/bin/env python
# (c) Aleksandar Topuzovic <Aleksandar.topuzovic@gmail.com>
class Solution:
    # @param an integer
    # @return a list of string
    def generateParenthesis(self, n):
        solutions = []
        OPEN_PARENTHESIS = '{'
        CLOSED_PARENTHESES = '}'

        def generator(curr, opened, closed, n, solutions):
            if closed == n:  # no more, add the string to results
                solutions.append(curr)
                return

            if opened < n:  # We can still open
                generator(curr + OPEN_PARENTHESIS, opened + 1, closed, n, solutions)

            if opened > closed:  # We should close some
                generator(curr + CLOSED_PARENTHESES, opened, closed + 1, n, solutions)

        generator('', 0, 0, n, solutions)
        return solutions


def main():
    s = Solution()
    print s.generateParenthesis(3)


if __name__ == '__main__':
    main()
