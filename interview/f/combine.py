#!/usr/bin/env python
# (c) Aleksandar Topuzovic <Aleksandar.topuzovic@gmail.com>
class Solution:
    # @return a list of lists of integers
    def combine(self, n, k):

        solutions = []
        def helper(curr, m):
            if len(curr) == k:
                solutions.append(curr)
                return

            for x in xrange(m+1, n+1):
                helper(curr + [x], x)

        helper([], 0)
        return solutions



def main():
    s = Solution()
    print s.combine(4, 2)


if __name__ == '__main__':
    main()
