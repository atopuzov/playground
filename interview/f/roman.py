#!/usr/bin/env python
# (c) Aleksandar Topuzovic <Aleksandar.topuzovic@gmail.com>
class Solution:
    # @return a string
    def intToRoman(self, num):
        values = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]
        codes = ['M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I']

        digits = []
        for value, code in zip(values,codes):
            while num >= value:
                digits.append(code)
                num -= value

        return ''.join(digits)

def main():
    s = Solution()
    print s.intToRoman(1976)


if __name__ == '__main__':
    main()
