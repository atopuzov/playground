#!/usr/bin/env python
# (c) Aleksandar Topuzovic <Aleksandar.topuzovic@gmail.com>
class Solution:
    # @return an integer
    def atoi(self, str):
        state = 0
        whitespace = (' ')
        numbers = {
            '0': 0,
            '1': 1,
            '2': 2,
            '3': 3,
            '4': 4,
            '5': 5,
            '6': 6,
            '7': 7,
            '8': 8,
            '9': 9
        }
        read = []
        positive = True
        for c in str:
            print '->', c
            if c in whitespace and state == 0:
                continue
            elif c == '+' and state == 0:
                state = 1  # Sign read
            elif c == '-' and state == 0:
                positive = False
                state = 1  # Sign read
            elif c in numbers and state in (0,1,2):
                read.append(numbers[c])
                state = 2 # Numbers
            else:
                break

        value = 0
        for i, n in enumerate(reversed(read)):
            value += 10**i * n

        if not positive:
            value = -value

        if value > 2147483647:
            return 2147483647
        elif value < -2147483648:
            return -2147483648
        return value


def main():
    s = Solution()
    x = '  -0012a42'
    print s.atoi(x)


if __name__ == '__main__':
    main()
