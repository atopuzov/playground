#!/usr/bin/env python
# (c) Aleksandar Topuzovic <Aleksandar.topuzovic@gmail.com>
class Solution:
    # @return a tuple, (index1, index2)
    def twoSum(self, num, target):
        ht = {}
        for i,e in enumerate(num):
            missing = target - e
            if missing in ht:
                j = ht[missing]
                if i<j:
                    return i, j
                else:
                    return j, i
            else:
                ht[e] = i


def main():
    s = Solution()
    l = [2, 7, 11, 15]
    t = 9

    # l = [3,2,4]
    # t = 6
    print s.twoSum(l, t)


if __name__ == '__main__':
    main()
