#!/usr/bin/env python
# (c) Aleksandar Topuzovic <Aleksandar.topuzovic@gmail.com>

class MinStack(object):
    def __init__(self):
        self.stack = []
        self.mins = []

    def push(self, value):
        if not self.stack:  # Empty stack
            self.stack.append(value)
            self.mins.append(value)
        else:
            smallest = self.mins[-1]  # Last element is the smallest
            if value <= smallest:
                self.mins.append(value)
            self.stack.append(value)

    def pop(self):
        if not self.stack:  # Empty stack
            return None
        else:
            value = self.stack.pop()
            if value == self.getMin():
                self.mins.pop()
            return value

    def getMin(self):
        if not self.stack:
            return None
        else:
            return self.mins[-1]

    def top(self):
        if not self.stack:
            return None
        else:
            return self.stack[-1]


def main():
    s = MinStack()
    s.push(10)
    print s.top(), s.getMin()
    s.push(2)
    print s.top(), s.getMin()
    s.push(5)
    print s.top(), s.getMin()
    s.push(1)
    print s.top(), s.getMin()
    print "->", s.pop()
    print s.top(), s.getMin()


if __name__ == '__main__':
    main()
