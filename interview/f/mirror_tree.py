#!/usr/bin/env python
# (c) Aleksandar Topuzovic <Aleksandar.topuzovic@gmail.com>
from collections import deque

class TreeNode(object):
    def __init__(self, value, left=None, right=None):
        self.value = value
        self.right = right
        self.left = left

    def __str__(self):
        return '{}'.format(self.value)

class Tree(object):
    def __init__(self, root=None):
        self.root = root

    def p(self):
        nodes = deque()
        nodes.append(self.root)

        while nodes:
            node = nodes.popleft()
            if node is None:
                continue
            print node,
            nodes.append(node.left)
            nodes.append(node.right)
        print

    def mirror(self):
        self._mirror(self.root)

    def _mirror(self, node):
        if node is None:
            return

        self._mirror(node.left)
        self._mirror(node.right)
        node.left, node.right = node.right, node.left

def build_sample_tree():
    n1 = TreeNode(1)
    n2 = TreeNode(2)
    n3 = TreeNode(3)
    n4 = TreeNode(4)
    n5 = TreeNode(5)
    n6 = TreeNode(6)
    n7 = TreeNode(7)
    n1.left = n2
    n1.right = n3
    n2.left = n4
    n2.right = n5
    n3.left = n6
    n3.right = n7
    return Tree(root=n1)

def main():
    t = build_sample_tree()
    t.p()
    t.mirror()
    t.p()


if __name__ == '__main__':
    main()
