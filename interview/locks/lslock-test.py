#!/usr/bin/python
# (c) 2014 Aleksandar Topuzovic <aleksandar.topuzovic@gmail.com>
from lslocks_imp import LSLocks
from multiprocessing import Queue, Process
import fcntl
import os
import tempfile
import time
import errno

TEST_DIR = '/tmp/lslock-test'
NUM_OF_FILES = 20

def worker(queue):
    '''
    Worker that will create a random lock file, and report it's pid and filename to master process
    :param queue: Communications queue
    :type queue: multiprocessing.Queue
    '''
    with tempfile.NamedTemporaryFile(suffix='.lock', dir=TEST_DIR) as lock:
        fcntl.flock(lock, fcntl.LOCK_EX)
        pid = os.getpid()
        name = lock.name
        queue.put((name, pid))
        while True:
            time.sleep(1)

            
def main():
    '''
    Main testing function
    '''
    # Make the test directory
    try:
        os.makedirs(TEST_DIR)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise e
        pass
    
    queue = Queue()

    # Fire up test processes (children)
    procs = []
    for _ in xrange(NUM_OF_FILES):
        p = Process(target=worker, args=(queue,))
        p.start()
        procs.append(p)

    # Get pids and filenames from children
    locks = {}
    for _ in xrange(NUM_OF_FILES):
        name, pid = queue.get()
        locks[pid] = {name}

    # Call our tool
    lslocks = LSLocks(TEST_DIR)
    get_locks = lslocks.get_locked_files()

    # Terminate all the processes
    for p in procs:
        p.terminate()

    # Verify the results
    if get_locks == locks:
        print "Test succesfull"
    else:
        print "It must be your fault! It worked for me(TM) ;-)"
         

if __name__ == '__main__':
    main()
