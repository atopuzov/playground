#!/usr/bin/python
# (c) 2014 Aleksandar Topuzovic <aleksandar.topuzovic@gmail.com>

from argparse import ArgumentParser
from lslocks_imp import LSLocks
from collections import defaultdict
import os
import logging

logger =  logging.getLogger(__name__)

def setup_argparser():
    parser = ArgumentParser(description='List locks and PIDs for files in a directory.')
    parser.add_argument('-d', '--directory', dest='directory', required=True, help='Directory to scan for locks')
    return parser.parse_args()

def main():
    args = setup_argparser()
    print "lslocks - list filenames and PIDs that lock the filename"
    lslocks = LSLocks(args.directory)
    locks = lslocks.get_locked_files()

    per_pid_report = {}
    
    for pid, files in locks.items():
        print "PID {}: files: {}".format(int(pid), ', '.join(files))

if __name__ == '__main__':
    main()
