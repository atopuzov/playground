# (c) 2014 Aleksandar Topuzovic <aleksandar.topuzovic@gmail.com>

from collections import defaultdict
import re
import os
import logging

logger =  logging.getLogger(__name__)

# Regular expression to match lines in /proc/locks
LOCKS_LINE = re.compile('''
^
(?P<num>\d+):
\s+
(?P<t1>FLOCK|POSIX)
\s+
(?P<t2>ADVISORY|MANDATORY|ADVISORY)
\s+
(?P<t3>READ|WRITE)
\s+
(?P<pid>\d+)  # process id
\s+
(?P<major>[0-9A-Fa-f]+)  # major device number
:
(?P<minor>[0-9A-Fa-f]+)  # minor device number
:
(?P<inum>\d+)  # inode number
\s+
(?P<start>\d+)
\s+
(?P<end>\d+|EOF)
$''', re.VERBOSE)

class LSLocks(object):
    '''
    Retrieves a list of locked files
    '''

    def __init__(self, dir):
        self.dir = dir
        self._locks = defaultdict(set)
        
    def get_locks_from_file(self, filename='/proc/locks'):
        '''
        Get's locks from file
        :param filename: Filename with locks
        :type filename: str
        '''
        with open(filename) as locks_file:
            self._parse_locks(locks_file)

    def _parse_locks(self, io):
        '''
        Parses locks file
        :param io: 
        :type io: file or stringio object
        '''
        for line in io:
            match = LOCKS_LINE.match(line)
            if match is not None:
                match_dict = match.groupdict()
                minor = int(match_dict['minor'], 16)
                major = int(match_dict['major'], 16)
                inum  = int(match_dict['inum'])
                pid   = int(match_dict['pid'])
                self._locks[(major, minor, inum)].add(pid)

    def _get_major_minor_inode_key(self, filename):
        '''
        Returns key based on major, minor mode and inode
        :param filename: Filename
        :type filename: str
        :rtype: tuple
        '''
        # Obtain device (minor, major) and i-node number from filename
        stat = os.stat(filename)
        inode = stat.st_ino
        device = stat.st_dev
        minor = os.minor(device)
        major = os.major(device)
    
        return (major, minor, inode)
                
    def _is_file_locked(self, filename):
        '''
        Determines if the filename is locked
        :param filename: Filename
        :type filename: str
        :rtype: bool
        '''
        key = self._get_major_minor_inode_key(filename)
        
        # Check if it's amongst the locked files
        if key in self._locks:
            return True
        return None

    def _get_pids_for_filename(self, filename):
        '''
        Returns the PIDs that are locking filename
        :param filename: Filename
        :type filename: str
        '''
        key = self._get_major_minor_inode_key(filename)

        return self._locks[key]

    def get_locked_files(self):
        '''
        Walks the directory and returns locked files
        :rtype: dict
        '''
        locked_files = defaultdict(set)
        self.get_locks_from_file()  # Fetch all the locks
        
        for dirpath, dirnames, filenames in os.walk(self.dir):
            logger.debug('Processing: {}'.format(dirpath))
            for filename in filenames:
                full_filename = os.path.join(dirpath, filename)
            
                if self._is_file_locked(full_filename):
                    for pid in self._get_pids_for_filename(full_filename):
                        locked_files[pid].add(full_filename)
                        
        return locked_files
