#!/usr/bin/env python
# (c) 2014 Aleksandar Topuzovic <aleksandar.topuzovic@gmail.com>
import random
import argparse


class Board(object):
    '''Represents a game board'''
    empty = '.'

    def __init__(self, n):
        '''
        :param n: Size of the board
        :type n: int
        '''
        self.n = n
        self.board = []
        self._initialize_board()

    def _initialize_board(self):
        '''
        Initializes the board
        '''
        self.board = [[None for _ in xrange(self.n)] for _ in xrange(self.n)]

    def __getitem__(self, key):
        '''
        :param key: Getting an element by key
        :type key: int
        '''
        return self.board[key]

    def __setitem__(self, key, value):
        '''
        :param key: Setting an element by key
        :type key: int
        '''
        self.board[key] = value

    def __str__(self):
        '''Represents the board as a string'''
        return '\n'.join(' '.join(x if x is not None else self.empty for x in line) for line in self.board)


class BattleShipsGame(object):
    ship_char = 'v'
    ship = {ship_char}
    bombed_hit = 'X'
    bombed_miss = 'o'

    def __init__(self, n=8, ship_length=3):
        self.n = n
        self.ship_length = ship_length
        self.board = Board(n)
        self.place_ship(self.ship_char, self.ship_length)
        self.bombed = 0

    def place_ship(self, ship_char, ship_length):
        '''
        Randomly places a ship
        :param ship_char: Charachter to represnt the ship
        :type ship_char: char
        :param ship_length: Lenght of the ship
        :type ship_length: int
        '''
        place_vertical = random.choice((True, False))
        delta = (0,1) if place_vertical else (1,0)

        # Maximum coordinates based on the ship placement
        xlimit = self.n if place_vertical else self.n - ship_length
        ylimit = self.n - ship_length if place_vertical else self.n

        # Starting coordinate
        start = (random.randrange(0, xlimit), random.randrange(0, ylimit))

        for _ in xrange(self.ship_length):
            self.board[start[0]][start[1]] = ship_char
            start = (start[0] + delta[0], start[1] + delta[1])


    def bomb_location(self, x, y):
        '''
        Bomb specified location
        :param x: X coordinate
        :type x: int
        :param y: Y coordinate
        :type y: int
        :return: Boolean indicating if bombing was succesfull
        :rtype: boolean
        '''
        self.bombed += 1
        bomb_succesfull = self.board[x][y] in self.ship
        self.board[x][y] = self.bombed_hit if bomb_succesfull else self.bombed_miss

        return bomb_succesfull


def play(game):
    print "Start board"
    print game.board

    print "Starting game"

    def detect_ship(x, y):
        # Seed for probing
        v_probes = [(0, -1), (0, 1)]
        h_probes = [(-1, 0), (1, 0)]
        # Chose if we first start vertically or horizontaly
        first_vertical = random.choice((True, False))
        probes = v_probes + h_probes if first_vertical else h_probes + v_probes

        start = (x,y)
        locations = [start]
        while probes and len(locations) < game.ship_length:
            probe = probes.pop()

            # Calculate new coordinates
            loc = tuple(map(lambda x,y: x+y, start, probe))

            # Verify new coordinates are valid:
            if 0 <= loc[0] < game.n and 0 <= loc[1] < game.n:
                bomb = game.bomb_location(loc[0], loc[1])  # Bomb the new location

                if bomb:  # if bombing is succesfull
                    locations.append(loc)
                    if probe[0] == 0:  # Move horizontally
                        delta = 1 if probe[1] > 0 else -1
                        new_probe = (0, probe[1] + delta)
                    elif probe[1] == 0:  # Move vertically
                        delta = 1 if probe[0] > 0 else -1
                        new_probe = (probe[0] + delta, 0)
                    probes.append(new_probe)  # Add new probe

        return locations

    # Generate possible locations
    possible_locations = []
    for x,y in ((a,b) for a in xrange(0, game.n) for b in xrange(a % game.ship_length, game.n, game.ship_length)):
        possible_locations.append((x,y))

    random.shuffle(possible_locations)

    # Start bombing locations
    for x,y in possible_locations:
        result = game.bomb_location(x, y)
        if result:  # If bombing succesfull move to search
            locations = detect_ship(x,y)
            break

    print game.board
    print 'Ship location: {}'.format(' '.join('{},{}'.format(l[0], l[1]) for l in locations))
    print 'Bombed times: {}'.format(game.bombed)
    print 'Bomb search locations: {}'.format(len(possible_locations))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', dest='ship_length', action='store', type=int, default=3, help='Ship\'s length')
    parser.add_argument('-n', dest='n', action='store', type=int, default=8, help='Board size')
    args = parser.parse_args()

    game = BattleShipsGame(args.n, args.ship_length)
    play(game)
