(* (c) Aleksandar Topuzovic <aleksandar.topuzovic@gmail.com> *)
fun suffixes [] = [[]]
  | suffixes xs = xs::suffixes(tl xs)

val test_suffixes = suffixes [1, 2 ,3, 4] = [[1,2,3,4], [2,3,4], [3,4], [4], []]
